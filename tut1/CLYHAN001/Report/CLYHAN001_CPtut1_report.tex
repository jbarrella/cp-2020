\documentclass[]{article}
\usepackage{graphicx}
\graphicspath{ {./Figures/} }
\usepackage{listings}
\usepackage[a4paper, total={7in, 10in}]{geometry}
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage{afterpage}
\usepackage{enumitem}
\usepackage{sidecap}
\usepackage{cite}


%opening
\title{PHY4000W Computational Physics: Tutorial 1}
\author{Hannah Clayton CLYHAN001}
\date{\parbox{\linewidth}{\centering\today\endgraf\smallskip}}

\begin{document}

\maketitle
	\renewcommand{\abstractname}{\vspace{-\baselineskip}}
	\begin{abstract}
	In this tutorial we investigated the time complexity of various matrix operations executed in Python. Python code using arrays and loops was written to compute the  dot product of two vectors, the multiplication of a matrix with a vector, the multiplication of two vectors and a matrix, and the multiplication of two matrices. The functions were tested using a given $n\times n$ matrix \textbf{\textit{M}} and $n$-dimensional vector \textbf{\textit{x}}. For each operation, the execution time was measured as a function of \textit{n}. These results were plotted and compared to the implementation of the same operations using the numpy package within Python. Finally, the expected asymptotic behaviour of each operation was discussed and compared to the observed behaviour. The loop implentations of each operation was shown to follow the expected asymptotic behabiour. 
	\end{abstract}

\section{Code}
	Methods for the matrix operations \textbf{\textit{x$^T$x}}, \textbf{\textit{Mx}}, \textbf{\textit{x$^T$Mx}} and \textbf{\textit{MM}} were implemented using arrays and loops in Python. The relevant functions can be found in \textit{src/MatrixOps.py}. Test methods to determine execution time as a function of $n$ can be found in \textit{src/testing.py}. The results were plotted using methods in \textit{src/plotting.py}. The numpy methods \textit{numpy.mathmul(a,b)} and \textit{numpy.dot(a,b)} were used for comparison. 

\section{Results}
	The values of \textbf{\textit{x$^T$x}} and \textbf{\textit{$x^T$Mx}} for $n = 5,10,20,100,1000$ were calculated and tabulated in Table~\ref{table:values}. The results were also printed to the $solutions.json$ file. 
	
	\begin{table}[h!]
		\centering
		\caption {Computed values for \textbf{\textit{x$^T$x}} and \textbf{\textit{$x^T$Mx}} for $n = 5,10,20,100,1000$}
		\smallskip
		\begin{tabular}{||c | c | c||} 
			\hline
			$n$ & \textbf{\textit{x$^T$x}} & \textbf{\textit{$x^T$Mx}} \\ [0.5ex] 
			\hline\hline
			5 & 55 & 225 \\
			\hline 
			10 & 385 & -1910 \\
			\hline 
			20 & 2870 & -9530 \\
			\hline 
			100 & 338350 & -1620350 \\
			\hline 
			1000 & 333833500 & -10566780541 \\
			\hline  
		\end{tabular}\label{table:values}
	\end{table}

	These results were checked and verified using the relevant functions in the Python numpy package. Addtional code was written using Mathematica to confirm some of the values for small $n$. It should be noted that due to the structure of the matrix objects and the nature of the calculations, integer results for \textbf{\textit{x$^T$x}} and \textbf{\textit{$x^T$Mx}} are expected for all values of $n$. However, some of the produced values were a very small fraction away from an integer. This deviation is likely due to precision errors at some points during the calculations. To correct for this, the obtained values were rounded to the nearest integer, and then the rounded values were presented as the final results of the calculations.
	
	\subsection{Expected asymptotic behaviour}
	In Table~\ref{table:bigO}, the expected time complexity of each of the implemented matrix operations is presented. 
	
	\begin{table}[h!]
		\centering
		\caption {The expected asymptotic behaviour of each of the implemented matrix operations}
		\smallskip
		\begin{tabular}{||c | c ||} 
			\hline
			Matrix operation & Expected asymptotic behaviour \\ [0.5ex] 
			\hline\hline
			\textbf{\textit{x$^T$x}} & $\mathcal{O}(n)$ \\
			\hline 
			\textbf{\textit{Mx}} & $\mathcal{O}(n^2)$ \\
			\hline 
			\textbf{\textit{x$^T$Mx}} & $\mathcal{O}(n^2)$ \\
			\hline 
			\textbf{\textit{MM}} & $\mathcal{O}(n^3)$ \\
			\hline 
		\end{tabular}\label{table:bigO}
	\end{table} 

	As an example, we consider the number of operations required for the multiplication of an $n\times n$ matrix \textbf{\textit{M}} with an $n$-dimensional vector \textbf{\textit{x}}. By definition of matrix multiplication, the $i$-th component of the resulting $n$-dimensional vector \textbf{\textit{Mx}} is given by
	\begin{equation}
	(Mx)^i = \sum_{j=1}^{N} M_{ij}x_j
	\end{equation} 
	This corresponds to $n$ componentwise multiplication operations, followed by $(n-1)$ additions. This has to be done $n$ times to produce the $n$ components in the final vector. Thus, the number of operations required is
	\begin{equation}
	\text{number of operations} = n(n+n-1) = 2n^2 - n
	\end{equation}
	This produces an expected time complexity of $\mathcal{O}(n^2)$.
	

\section{Execution time}
	The execution times of each of the matrix operations were measured for both the loop and numpy implementations. For \textbf{\textit{x$^T$x}}, \textbf{\textit{Mx}} and \textbf{\textit{x$^T$Mx}}, problem sizes of $n=5,10,20,100$ and 1000 were tested. Due to long runtimes of the loop implementation for \textbf{\textit{MM}} for large $n$, smaller test values for \textbf{\textit{MM}} were used ($n= 5,10,20,50,100,200,300$). 
				
	These execution time results were plotted as a function of $n$ in Figures~\ref{fig:dot}-\ref{fig:MM}. The results for the loop and numpy implementations are shown together in each left figure, while the results for the numpy implementation alone were shown in the plots on the right. In all cases, the numpy methods were significantly faster than the loop implementations. For $n=1000$, the numpy implementation was one order of magnitude faster for \textbf{\textit{x$^T$x}}, and three orders of magnitude faster for \textbf{\textit{Mx}} and \textbf{\textit{x$^T$Mx}}. 
	
	The (appropriately-scaled) functions which represent the expected asymptotic behaviour (discussed in the previous section) were included for comparison. For each operation, the loop implementation matched the expected asymptotic behaviour well. Except for \textbf{\textit{MM}}, the numpy implementations did not seem to follow the expected asymptotic behaviour. It is likely that specialised, more efficient algorithms have been used for those methods to reduce the complexity. 
	
	\subsection{Plots}
		\begin{figure}[h]
			\begin{minipage}[c]{0.48\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/dotProductCombined.png}
			\end{minipage}
			\begin{minipage}[c]{0.5\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/dotNumpy.png}
			\end{minipage}\hfill
			\caption{The execution time (as a function of problem size $n$) for the dot product of two vectors \textbf{\textit{x$^T$x}} for the loop and numpy implementations.}\label{fig:dot}
		\end{figure}\label{(fig:dot)}
	
		\begin{figure}[h]
			\begin{minipage}[c]{0.5\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/MxCombined.png}
			\end{minipage}
			\begin{minipage}[c]{0.5\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/MxNumpy.png}
			\end{minipage}\hfill
			\caption{The execution time (as a function of problem size $n$) for the  multiplication of a matrix with a vector \textbf{\textit{Mx}} for the loop and numpy implementations.}\label{fig:Mx}
		\end{figure}\label{(fig:Mx)}
	
		\begin{figure}[h]
			\begin{minipage}[c]{0.49\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/xMxCombined.png}
			\end{minipage}
			\begin{minipage}[c]{0.5\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/xMxNumpy.png}
			\end{minipage}\hfill
			\caption{The execution time (as a function of problem size $n$) for the  multiplication of two vectors and a matrix \textbf{\textit{x$^T$Mx}} for the loop and numpy implementations.}\label{fig:xMx}
		\end{figure}\label{(fig:xMx)}
	
		\begin{figure}[h]
			\begin{minipage}[c]{0.49\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/MMCombined.png}
			\end{minipage}
			\begin{minipage}[c]{0.5\textwidth}
				\includegraphics[width=\textwidth]{/Users/hannahclayton/Documents/PHY4000W/CP/Assignments/compphys2020/tut1/CLYHAN001/Figures/MMNumpy.png}
			\end{minipage}\hfill
			\caption{The execution time (as a function of problem size $n$) for the multiplication of two matrices \textbf{\textit{MM}} for the loop and numpy implementations.}\label{fig:MM}
		\end{figure}\label{(fig:MM)}
	
	\clearpage

	\subsection{Estimation of number of FLOPS of my system}
	The number of floating point operations per second (FLOPS) of a system can be estimated by dividing the estimated number of operations in a code block by the execution time for the block. 
	
	In the dot product loop implementation, the expected number of floating point operations is $(2n-1)$ for $n$-dimensional vectors. The code \textit{src/FLOPSestimate.py} was written to compute the execution time for $n=5,10,20,100$ and 1000. The result for each $n$ was computed 5 times and averaged. The obtained execution times were divided by $2n-1$. Finally all of these results were averaged to obtain an estimate for the number of FLOPS of my system as 2.54 MHz.
	
	\section{Conclusions}
	This report explored the time complexity of various matrix operations, and compared the execution times of self-written functions with relevant numpy methods for different problem sizes. It is clear that the methods from Python packages like numpy are far more efficient than code written using the iterative matrix operation definitions. Where possible, one should opt to use pre-existing packages, as these have been specially optimised for efficiency in most cases.


\end{document}