import json
import sys
import os


if len(sys.argv) != 2:
    exit('usage: complete-checker.py <tutnumber e.g. "tut1">')

tut = sys.argv[1]

#load true answer json#
def load_true_answers():
    num_questions = 0
    with open('{}/data/solutions.json'.format(tut)) as json_file:       
        answers = json.load(json_file)
    for v in answers.values():
        if isinstance(v, dict):
            for vv in v.values():
                if isinstance(vv, dict):
                    print('too many levels in sample-solutions.json - number of questions set to 10')
                    num_questions = 10
                    break
                if vv is not None:
                    num_questions += 1
        elif v is not None:
            num_questions += 1
    return answers, num_questions


#loop through student directories. Execute and grade#
def find_and_run():
    tut_path = tut + '/'
    student_folders = os.listdir(tut_path)
    student_folders.remove('data')
    for student in student_folders:
        files = os.listdir(tut_path + student)
        for f in files:
            if not f.endswith('.py'):
                files.remove(f)
        if len(files) == 0:
            continue
        for f in files:
            os.system('time python {}/{}/{}'.format(tut, student, f))
        json_path = os.popen("find . -name '*.json' ! '(' -wholename '*/data*' ')'").read().strip()
        score = compare(answers, json_path)
        print('{} - {}/{}'.format(student, score, num_questions))
        os.system('rm %s' % (json_path))


#compare student answers against true answers#
def compare(answers, json_path):
    with open(json_path) as json_file:
        student_answers = json.load(json_file)
    score = 0  
    for k, v in answers.items():
        if isinstance(v, dict):
            for kk, vv in v.items():
                if student_answers[k][kk] == vv:
                    score += 1
        else: 
            if student_answers[k] == v:
                score+=1 
    return score          


if __name__ == "__main__":
    answers, num_questions = load_true_answers()
    find_and_run()
