import json
import matplotlib.pyplot as plt
import numpy as np

# this file plots each relevant timing figure using the json output of Part2

def plotFigure(xdata, ydata, title, savefile):
    plt.figure()
    plt.plot(xdata, ydata, 'ro')
    if 'xtx' in title:  # n values too large to read, so rotate axis
        plt.xticks(rotation=45)
    plt.title(title)
    plt.xlabel("Dimension, n")
    plt.ylabel("Time (s)")
    plt.tight_layout()
    plt.savefig(title + ".png", bbox_inches = "tight")


def plot(numpyType, typeData):
    if "noNumpy" in numpyType:
        parttitle ="Python calculations"
    else:
        parttitle = "Numpy calculations"
    for operation, opTimings in typeData.items():
        title = parttitle + " " + operation
        plotFigure(np.asarray(list(opTimings.keys())), np.asarray(list(opTimings.values())), title, None)  # plots the time taken for each operation against dimensions (n) 
        
    

def plotFigures(data):
    for numpytype, typeData in data.items():
        plot(numpytype, typeData)  # plots for numpy and non numpy results
    
    
def run():
    with open("timings.txt", 'r') as f:
        data = json.load(f)  # loads timing data
    plotFigures(data)
