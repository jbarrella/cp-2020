# Contains methods to prepare execution time plots for each of the operations

import numpy as np
from matplotlib import pyplot as plt

# dot product
def plotDotExecutionTime(nData, tData, nptData):
	x = np.linspace(0,1000,100)

	plt.plot(x, (tData[-1]/1000)*x, '--', label='O(n)')
	plt.plot(nData,tData, 'bx', label='Loop implementation')
	plt.plot(nData, nptData, 'rx', label='Numpy implementation')
	plt.xscale('log')
	plt.title("Execution time for dot product of two Nx1-dimensional vectors")
	plt.ylabel("Execution time (s)")
	plt.xlabel("N")
	plt.legend()

	plt.savefig('dotPlot')
	plt.clf()

# Mx
def plotMxExecutionTime(nData, tData, nptData):
	x = np.linspace(0,1000,100)
	y = (tData[-1]/(10**6))*x*x

	plt.plot(x, y, '--', label=r'O($n^2$)')
	plt.plot(nData,tData, 'bx', label='Loop implementation')
	plt.plot(nData, nptData, 'rx', label='Numpy implementation')
	plt.xscale('log')
	plt.title("Execution time for multiplication of a matrix (NxN) with a vector (Nx1)")
	plt.ylabel("Execution time (s)")
	plt.xlabel("N")
	plt.legend()

	plt.savefig('MxPlot')
	plt.clf()

# xMx
def plotxMxExecutionTime(nData, tData, nptData):
	x = np.linspace(0,1000,100)
	y = (tData[-1]/(10**6))*x*x

	plt.plot(x, y, '--', label=r'O($n^2$)')
	plt.plot(nData,tData, 'bx', label='Loop implementation')
	plt.plot(nData, nptData, 'rx', label='Numpy implementation')
	plt.xscale('log')
	plt.title('Execution time for multiplication of a matrix (NxN) and two vectors (Nx1) \n x$^T$Mx')
	plt.ylabel("Execution time (s)")
	plt.xlabel("N")
	plt.legend()

	plt.savefig('xMxPlot')
	plt.clf()

# MM
def plotMMExecutionTime(nData, tData, nptData):
	x = np.linspace(0,nData[-1],100)
	y = (tData[-1]/(nData[-1]**3))*x*x*x

	plt.plot(x, y, '--', label=r'O($n^3$)')
	plt.plot(nData,tData, 'bx', label='Loop implementation')
	plt.plot(nData, nptData, 'rx', label='Numpy implementation')
	plt.xscale('log')
	plt.title('Execution time for multiplication of two NxN matrices, MM')
	plt.ylabel("Execution time (s)")
	plt.xlabel("N")
	plt.legend()

	plt.savefig('MMPlot')
	plt.clf()
	