import numpy as np
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import json

#Matrix bigM
def create_M(e):
    x = np.arange(1, e+1)
    y = np.arange(1, e+1)
    xx, yy = np.meshgrid(x,y)
    M = ((37.1*xx + 91.7*yy**2) % 20) - 10
    return M

#Vector I_ij
def create_x(e):
    vect = []
    for i in range(e):
        vect.append(i + 1 )
    return vect

print(create_x(20))

#Manual version of taking the dot product of two vectors
def dot_product_of_two_vectors(a,b):
    addition = 0
    for i in range(len(a)):
        dot_product = a[i]*b[i]
        addition = addition + dot_product
    return addition

#Multipliplication of a matrix with a vector
def matrix_with_a_vector(m,b):
    y = []
    for r in range(len(m[0])):
        dot_product = dot_product_of_two_vectors(m[r], b)
        y.append(dot_product)
    return y

#Mulitplicationof two vectors and a matrix
def two_vectors_and_a_matrix(matrix,vector):
    step1 = matrix_with_a_vector(matrix, vector)
    step2 = dot_product_of_two_vectors(vector, step1)
    return step2

#Multiplication of two matrices
def column(matrix, col_number):
    col_vect = []
    for row in matrix:
        col_vect.append(row[col_number])
    return col_vect

def two_matrices(Lm,Lr):
    resultant_matrix = []
    for i in range(len(Lm)):
        row = []
        for j in range(len(Lr)):
            right_col = column(Lr, j)
            row.append(dot_product_of_two_vectors(Lm[i], right_col))
        resultant_matrix.append(row)
    return resultant_matrix

#Time stuff - I'm assuming we can use numpy for the timer
def get_times(ns, function_name):
    times = np.zeros((ns.shape))
    for idx, n in enumerate(ns):
        x = create_x(n)
        M = create_M(n)
        tot_time = 0
        loops = 5
        for i in range(loops):
            start = timer()
            if function_name == "my_dot":
                dot_product_of_two_vectors(x,x)
            elif function_name == "my_mat_vect":
                matrix_with_a_vector(M,x)
            elif function_name == "my_vect_mat_vect":
                two_vectors_and_a_matrix(M,x)
            elif function_name == "my_mat_mult":
                two_matrices(M,M)
            elif function_name == "np_dot":
                np.dot(x, x)
            elif function_name == "np_mat_vect":
                np.dot(M,x)
            elif function_name == "np_vect_mat_vect":
                np.dot(x, np.matmul(M,x))
            elif function_name == "np_mat_mult":
                np.matmul(M,M)
            end = timer()
            tot_time += end-start
        times[idx] = tot_time / loops
    return times
 

#Attempting to make a .json file output of results

dimensions = [5, 10, 20, 100, 1000]

xtx_data = []
xtMx_data = []

for dim in dimensions:
	x = create_x(dim)
	M = create_M(dim)
	xtx = dot_product_of_two_vectors(x, x)
	xtMx = dot_product_of_two_vectors(x, matrix_with_a_vector(M, x))
	xtx_data.append(xtx)
	xtMx_data.append(xtMx)


data = {}
xtx = {}
xtMx = {}
for length in range(len(dimensions)):
    xtx.update({str(dimensions[length]): xtx_data[length]})
    xtMx.update({str(dimensions[length]): xtMx_data[length]})


data.update({"xtx": xtx})
data.update({"xtMx": xtMx})

with open('pllvin013.json', 'w') as f:
    json.dump(data, f)



#Plot comparison for dot product of two vectors
ns = np.arange(10, 200, 10)
my_dot = get_times(ns, "my_dot")
np_dot = get_times(ns, "np_dot")

plt.plot(ns, my_dot, "o-", label = "Manual")
plt.plot(ns, np_dot, "o-", label = "Numpy")
plt.legend()
plt.xlabel("Dimensions n")
plt.ylabel("Times in s")
plt.title("Dot product comparison $x^{T}x$")
plt.show()

#Plot comparison for multiplication of a vector with a matrix
ns = np.arange(10, 200, 10)
my_mat_vect = get_times(ns, "my_mat_vect")
np_mat_vect = get_times(ns, "np_mat_vect")

plt.plot(ns, my_mat_vect, "o-", label = "Manual")
plt.plot(ns, np_mat_vect, "o-", label = "Numpy")
plt.legend()
plt.xlabel("Dimensions n")
plt.ylabel("Times in s")
plt.title("Muliplication of a matrix with a vector comparison $Mx$")
plt.show()

#Plot comparison for multiplication of two vectors and a matrix
ns = np.arange(10, 200, 10)
my_vect_mat_vect = get_times(ns, "my_vect_mat_vect")
np_vect_mat_vect = get_times(ns, "np_vect_mat_vect")

plt.plot(ns, my_vect_mat_vect, "o-", label = "Manual")
plt.plot(ns, np_vect_mat_vect, "o-", label = "Numpy")
plt.legend()
plt.xlabel("Dimensions n")
plt.ylabel("Times in s")
plt.title("Mulitplication of two vectors and a matrix $x^{T}Mx$")
plt.show()

#Plot comparison for multiplication of two matrices
ns = np.arange(10, 100, 10)
my_mat_mult = get_times(ns, "my_mat_mult")
np_mat_mult = get_times(ns, "np_mat_mult")

plt.plot(ns, my_mat_mult, "o-", label = "Manual")
plt.plot(ns, np_mat_mult, "o-", label = "Numpy")
plt.legend()
plt.xlabel("Dimensions n")
plt.ylabel("Times in s")
plt.title("Mulitplication of two matrices $MM$")
plt.show()