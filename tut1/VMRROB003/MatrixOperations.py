import time
import json
import numpy as np


#UTILITY FUNCTIONS
def timed_func(func, params):  #determines the runtime of a given function with given parameters
    start_time = time.time()
    value = func(*params)  # runs the function
    time_elapsed = time.time() - start_time
    return value, time_elapsed

def write_json(data, filename = "solutions.json"):  # writes a dictionary to a json file
    with open(filename, 'w') as outfile:
        json.dump(data, outfile)
    
def load_json(filename = "solutions.json"):  # loads a json file into a dictionary
    data = json.load(filename)
    return data

#SETUP FUNCTIONS
    
def setupx(n):  # sets up x as x_i=i
    x = [i+1 for i in range(n)]
    return x

def setupM(n):  # sets up M as specified in brief
    M = [[(((float(37.1)*i+float(91.7)*(j**2))%20)-10) for j in range(1,n+1)] for i in range(1,n+1)] 
    return M

def setupMNumpy(n):  # sets up M using numpy arrays
    M = np.zeros((n,n))
    for i in range(1,n+1):
        for j in range(1,n+1):
            M[i-1][j-1] = (((37.1*i+91.7*j**2)%20)-10)
    return M

def setupxNumpy(n):  # sets up x using a numpy function
    x = np.arange(1,n+1)
    return x

#CALCULATION FUNCTIONS
    
#Functions are named according to order of multiplications
#ie matrixvector(M,x) = Mx

def matrixvector(M,x):  # calculates Mx
    result = [0]*len(x)  # result should be vector of dim(x)
    for i in range(len(x)):
        for j in range(len(x)):
            result[i] += M[i][j]*x[j]
    return result

def vectorvector(x,y):  # calculates xTy (xTx in our implementations)
    value = 0 # returns a scalar
    for i in range(len(x)):
        value += x[i]*y[i]
    return value

def matrixmatrix(M,N):  # calculates MN (or MM in our implementation)
    dim = len(M)
    result = [[0 for i in range(dim)] for j in range(dim)]  # result should be 2D matrix of dimension dim(M)xdim(N)
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                result[i][j] += M[i][k] * N[k][j]
    return result

def vectormatrixvector(x,M,y):  # calculates xTMy using previously defined functions
    return vectorvector(x, matrixvector(M,y))

def vectormatrixvectornumpy(x,M,y):  # calculates xTmy using numpy dot function
    return np.dot(x, np.dot(M,x))   



