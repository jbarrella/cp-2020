# CP 2020

Tutor Info:
 
Jason Barrella  
brrjas002@myuct.ac.za  
RW James 3.10

## Introduction

This readme serves to introduce you to the functionality of the CP 2020 repository and explain the basic development workflow. This repo will hold all of the assessed code written by the 2020 class and facilitate the automatic grading of some assignments.

## Git Basics

We will be using [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) as our version control system so install that if you don't have it already.

Begin by cloning the repo.

```
git clone https://bitbucket.org/tdietel/compphys2020.git
```

This will create an identical copy of the repo on the your local machine. For each new tutorial, you should create a new branch. This will get merged later into the master branch and then deleted after you have submitted your work. To create a new branch, use:

```
git checkout -b '<nameofbranch>'
```

You can name your branch whatever you like, but it needs to be unique. This command will both create and then checkout the new branch. After checking out a branch, any subsequent commits you make will affect that branch only. To return to the master branch, use `git checkout master`.

Now you can begin to work locally on your machine and solve the tutorial. Make sure you do all your work in the folder provided for you. This will help to keep things neat when all the branches are merged together. Use,

```
git add --all
git commit -m '<message describing commit>'
git push
```

to first stage all the changes you made locally, then push those changes to the remote repo. Remember to frequently commit and push your work to your branch. You won't have rights to commit directly to the master branch, but you can make as many commits as you want to your own branch.

## Completing a Tutorial

For each tutorial, your work will be done in the directory root/<tutnumber>/<studentnumber>. All outputs produced by your code e.g. plots, numerical answers, etc need to be written to disk by your code so that we can collect them from the repo. Simply printing the answers will not be enough. The preferred format of numerical answers will be json files since these are easily machine readable by the grading scripts. You can solve the tutorial by writing multiple scripts/modules, but **your code should output only one json file**. It may output an indefinite number of other files in addition to the one json.

Please name your files **without the use of whitespaces**. Instead, use underscores, hyphens, etc.

In /<tutnumber>/data will be, among other things, certain files you will need to complete each tutorial succesfully:

* input.json | This will contain various parameters to use as inputs when running your code.
* sample-solutions.json | This contains an example of how your answer json should look. It might be a good idea for your code to read this in and then change the values to your answers.

In addiition to housing all the code for CP 2020, this repo will also serve as a controlled environment to run your code. The repo will run your code using a [Docker](https://www.docker.com/) image with pre-installed packages and programming languages. The image is currently configured with python:3.7.3 + numpy:1.16, matplotlib:3.1.2, scipy:1.4.1. This means that you need to write your code so that it will run in this environment. If you would like to use an additional package with your program, kindly email your request to me and I'll see what I can do.

## Submitting your Work

Your final solution will be submitted in the form of a pull request made to the master branch. This should be done after you have pushed all your final changes to your own tutorial branch. A pull request is best made using the Bitbucket web GUI from the repo page. All that you need to submit is your code, the outputs will be generated when the repo runs it.

When your create your pull request, the repo will automatically execute your code and gather the outputs. You will be able to view the results of this run in the pipelines window on the Bitbucket repo web page. It will also display step by step information so, if anything goes wrong, you should be able to figure out where.

If your code does not run as planned, simply make the necessary modifications to your local files, and push these changes to your branch. Your pull request will automatically be updated with the new content of your branch and the pipeline will run again. This can be done as many times as needed.
