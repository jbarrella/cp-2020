#!/bin/bash


TUT_NUMBER=tut2
# TUT_NUMBER=$(git show --format=format:%an --dirstat $BITBUCKET_COMMIT | cut -d/ -f1 | cut -d$'\n' -f2)
AUTHOR_STUDENT_NUMBER=$(git show --format=format:%an -s $BITBUCKET_COMMIT | cut -d@ -f1)
AUTHOR_FOLDER=$(git show --format=format:"" --dirstat $BITBUCKET_COMMIT | cut -d/ -f2)

printf "$TUT_NUMBER \n"
printf "\nYour working directory is, \n"
git show --format=format:%an --dirstat $BITBUCKET_COMMIT

printf "\nPipeline is searching your folder in, \n"
printf "$AUTHOR_FOLDER \n \n"


if [ -f ${TUT_NUMBER}/${AUTHOR_FOLDER}/run.sh ]
then
    echo running your shell script at ${TUT_NUMBER}/${AUTHOR_FOLDER} && sh ${TUT_NUMBER}/${AUTHOR_FOLDER}/run.sh
else
    echo no shell script found,
    for f in $(find ${TUT_NUMBER}/${AUTHOR_FOLDER} -name '*.py')
    do 
        echo running $f && python $f
    done
fi
