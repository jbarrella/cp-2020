# Estimates the FLOPS of my system using the loop 
import numpy as np
import MatrixOps
import time
from matplotlib import pyplot as plt
import testing

def executionTime(nData):
	numTests = 5
	loopTimes = np.zeros(len(nData))

	for i in range(numTests):
		testLoopTimes = []

		for N in nData:
			M = MatrixOps.createMatrix(N)
			x = MatrixOps.createVec(N)

			# time own implementation
			t0 = time.perf_counter()
			MatrixOps.dot(x,x)
			testLoopTimes.append(time.perf_counter()-t0)


		testLoopTimes = np.asarray(testLoopTimes)
		loopTimes += testLoopTimes

	loopTimes = loopTimes/float(numTests)
	print(loopTimes)

	for i in range(len(nData)):
		loopTimes[i] = (2*nData[i]-1)/float(loopTimes[i])

	print(loopTimes)
	print(np.average(loopTimes))

executionTime([5,10,20,100,1000])

