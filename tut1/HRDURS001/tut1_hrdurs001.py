#HRDURS001 CP TUT 1 PHY4000W 02/2020
#
#This code runs over a loop of n for n given by sizes
#All plots are compiled when all data is appended to the arrays. 
# For the matrix defined by f, i and j are indexed from 1 (as this would be assumed by a human being )
#IMPORTS
import json
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np 
import scipy as sp
import timeit
from timeit import default_timer as timer
import matplotlib.font_manager as font_manager

#definitions below imports

#'Manual' Matrix Multiplication 
def mult(X,Y):
		RES=[0]*len(Y)
		for x in range(len(RES)):
			RES[x]=[0]*len(X[0])
		# iterate through rows of X
		for i in range(len(X)):
		# iterate through columns of Y
			for j in range(len(Y[0])):
				# iterate through rows of Y
				for k in range(len(Y)):
					RES[i][j] += X[i][k] * Y[k][j]
		return RES

#Writing of a Json file
def write_json(my_dic):
	f=open("HRDURS001_tut1.json","w") #name of file
	new_json=json.dumps(my_dic)
	f.write(new_json)			#rewritten after each build
	f.close()

#Font styles for Plots
font1 = font_manager.FontProperties(family='serif', 
								   weight='normal',
								   style='normal', size=12)
font = {'family': 'serif',
		'color':  'black',
		'weight': 'normal',
		'size': 12,
		}
#Empty arrays for appended time values 
#calculations=['xtx', 'Mx', 'xtMx', 'MM']--- This should explain the suffixing of the timing arrays
#manualtime= from mult def above
#nptime= Time ffor np.matmult to derive the same solution as def mult

manualtimextx=[]
nptimextx=[]
manualtimextmx=[]
nptimextmx=[]
manualtimemx=[]
nptimemx=[]
manualtimemm=[]
nptimemm=[]

answers={} #initial Dictionary 

size=[5, 10, 20, 100, 1000] #n values

answers["xtx"]={} #Json file dictionaries
answers["xtMx"]={}

for k in size: #Begining of loop

	n=k
	matrix=np.zeros((n,n)) # nxn matrix
	x=np.zeros((n,1))      # nx1 matrix


	#MATRIX M entries for indexes i and j
	def f(i,j):
		return np.mod((37.1*i)+(91.7*(j**2)), 20) -10 

	#python indexes from 0 so i-1 indicates 1st element 
	for i in range(1,n+1):     #Indexing over Rows
		
		for j in range(1,n+1):  #Indexing over columns
			matrix[i-1][j-1]=f(i,j)
		x[i-1]=i   


	#CALCULATION OF XtX
								#The following comments are representative of the rest of the loop (since there is repetition for each calcultation)
	start = timer()				#begin the timer
	calxtx=mult(x.T,x)[0][0]	#calculation
	end = timer()				#end timer  

	manualtimextx.append(end-start) #Append time value to array
								# for the rest of the code- see comments on lines 90-94 to understand 
	start=timer()
	man=np.matmul(x.T,x)
	end=timer()

	nptimextx.append(end-start)

	answers["xtx"][str(k)]=calxtx #Sent to json


	#Calculation of xtmx	
	start = timer()
	calmx=mult(matrix, x)
	calxtmx=mult(x.T, calmx)[0][0]

	end = timer()

	

	manualtimextmx.append(end-start)
	
	 

	start=timer()
	man=np.matmul(matrix,x)
	man1=np.matmul(x.T,man)
	end=timer()

	nptimextmx.append(end-start)
	

	answers["xtMx"][str(k)]=calxtmx






	#calculation of Mx
	start = timer()
	calmx2=mult(matrix, x)
	end = timer()

	
	manualtimemx.append(end-start)
	
	 
	start=timer()
	man=np.matmul(matrix,x)
	end=timer()

	nptimemx.append(end-start)
	
	#Calculation of MM

	start = timer()
	calmx2=mult(matrix, matrix)
	end = timer()

	
	manualtimemm.append(end-start)
	
	 
	start=timer()
	man=np.matmul(matrix,matrix)
	end=timer()

	nptimemm.append(end-start)



write_json(answers) #All answers written in Json file





#NORMAL SCALED PLOTS

#Plot for xtx
plt.title('x.Tx', fontdict=font)
plt.plot(size, manualtimextx, 'r--')
plt.plot(size, nptimextx, 'b--')
plt.plot(size, manualtimextx, 'ro', label=r'Manual Code time')
plt.plot(size, nptimextx, 'bo', label='Numpy Package time')
plt.legend()
#plt.yscale('log')
plt.ylabel('Computation Time (s)', fontdict=font)
plt.xlabel('n value', fontdict=font)
plt.xlim(0)

plt.show()
		
#plotfor xtmx
plt.title('xtMx', fontdict=font)
plt.plot(size, manualtimextmx, 'r--')
plt.plot(size, nptimextmx, 'b--')
plt.plot(size, manualtimextmx, 'ro', label='Manual Code')
plt.plot(size, nptimextmx, 'bo', label='Numpy Package code')
plt.legend()
plt.ylabel('Computation Time (s)', fontdict=font)
plt.xlabel('n value', fontdict=font)

plt.xlim(0)

plt.show()
		
#plot for mx
plt.title('Mx ', fontdict=font)
plt.plot(size, manualtimemx, 'r--')
plt.plot(size, nptimemx, 'b--')
plt.plot(size, manualtimemx, 'ro', label='Manual Code')
plt.plot(size, nptimemx, 'bo', label='Numpy Package code')
plt.legend()
plt.ylabel('Computation Time (s)', fontdict=font)
plt.xlabel('n value', fontdict=font)

plt.xlim(0)

plt.show()
		
#plot for mm
plt.title('MM ', fontdict=font)
plt.plot(size, manualtimemm, 'r--')
plt.plot(size, nptimemm, 'b--')
plt.plot(size, manualtimemm, 'ro', label='Manual Code')
plt.plot(size, nptimemm, 'bo', label='Numpy Package code')
plt.legend()
plt.ylabel('Computation Time (s)', fontdict=font)
plt.xlabel('n value', fontdict=font)
plt.xlim(0)
plt.show()
		


#LOGARITHMIC PLOTS
#Plot for xtx
plt.title('xtx ', fontdict=font)
plt.plot(size, manualtimextx, 'r--')
plt.plot(size, nptimextx, 'b--')
plt.plot(size, manualtimextx, 'ro', label=r'Manual Code time')
plt.plot(size, nptimextx, 'bo', label='Numpy Package time')
plt.legend()
plt.yscale('log')
plt.ylabel('Computation Time (s) (Log scaled)', fontdict=font)
plt.xlabel('n value', fontdict=font)
plt.xlim(0)
plt.show()
		
#plotfor xtmx
plt.title('xtMx ', fontdict=font)
plt.plot(size, manualtimextmx, 'r--')
plt.plot(size, nptimextmx, 'b--')
plt.plot(size, manualtimextmx, 'ro', label='Manual Code')
plt.plot(size, nptimextmx, 'bo', label='Numpy Package code')
plt.legend()
plt.yscale('log')
plt.ylabel('Computation Time (s) (Log scaled)', fontdict=font)
plt.xlabel('n value', fontdict=font)
plt.xlim(0)
plt.show()
		
#plot for mx
plt.title('Mx ', fontdict=font)
plt.plot(size, manualtimemx, 'r--')
plt.plot(size, nptimemx, 'b--')
plt.plot(size, manualtimemx, 'ro', label='Manual Code')
plt.plot(size, nptimemx, 'bo', label='Numpy Package code')
plt.legend()
plt.yscale('log')
plt.ylabel('Computation Time (s) (Log scaled)', fontdict=font)
plt.xlabel('n value', fontdict=font)
plt.xlim(0)
plt.show()
		
#plot for mm
plt.title('MM ', fontdict=font)
plt.plot(size, manualtimemm, 'r--')
plt.plot(size, nptimemm, 'b--')
plt.plot(size, manualtimemm, 'ro', label='Manual Code')
plt.plot(size, nptimemm, 'bo', label='Numpy Package code')
plt.legend()
plt.yscale('log')
plt.ylabel('Computation Time (s) (Log scaled)', fontdict=font)
plt.xlabel('n value', fontdict=font)
plt.xlim(0)
plt.show()
		

#The plots bellow were generated as an after thought for final submission
#Comparison plots
a=np.linspace(0,1000,100)
b=np.zeros(100)
c=np.zeros(100)
d=np.zeros(100)

for g in range(len(a)):
	b[g]=np.log(a[g])
	c[g]=2*np.log(a[g])
	d[g]=3*np.log(a[g])

plt.plot(a,b, label='ln(n)')
plt.plot(a,c,label='2ln(n)')
plt.plot(a,d, label='3ln(n)')
plt.legend()
plt.ylabel('Functions', fontdict=font)
plt.xlabel('n', fontdict=font)
plt.show()


plt.plot(size, manualtimextx, label='xtx')
plt.plot(size, manualtimextmx, label='xtmx')
plt.plot(size, manualtimemx, label='mx')
plt.plot(size, manualtimemm, label='mm')
plt.ylabel('time (s), log scale ', fontdict=font)
plt.xlabel('n', fontdict=font)
plt.yscale('log')
plt.legend()
plt.show()


plt.plot(size, nptimextx, label='xtx')
plt.plot(size, nptimextmx, label='xtmx')
plt.plot(size, nptimemx, label='mx')
plt.plot(size, nptimemm, label='mm')
plt.ylabel('time (s), log scale ', fontdict=font)
plt.xlabel('n', fontdict=font)
plt.yscale('log')
plt.legend()
plt.show()

