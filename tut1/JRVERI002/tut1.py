# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
#Computational Physics Tut1 - Matrices and vectors
#import timeit
import time
import numpy as np
import json
import matplotlib.pyplot as plt


######These matrices and Vectors will be used for all excecutions######

        
#creating matrix M

   
def M(n):
    m=np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            m[i][j]=((37.1*i+91.7*j**2)%20.0)-10
    return m



#creating 1xn dimensional row vector (Xt=transpose of vector)
def Xt(n):
    x=[]
    for i in range(n):
        x.append(i)
    return np.array([x])


#creating nx1 dimension column vector that is transpose of xt
def X(x):
    a=np.shape(x)
    return x.reshape(a[1],1)





#########Matrix Operations- without numpy########

#dot product - returns a number
def dot(a,b):
    n=0
    c=np.shape(a)[1]
    for i in range(c):
        n+=a[0][i]*b[i][0]  
    return n


#matrix multiplication, multiplication of a axb and bxc matrix withh return a axc matrix

def matrixmult(a,b):
    sa=np.shape(a)
    sb=np.shape(b)
    if sa[1]!=sb[0]:    ##making sure the dimensions alow for matrix multiplication
        return "Incompatable Dimensions"
    else:
        c=np.zeros((sa[0],sb[1]))
        for i in range(sa[0]):
            for j in range(sb[1]):
                for k in range(sa[1]):
                    c[i][j]+=a[i][k]*b[k][j]
        return c
    
    
    
########### Executing the matrix operations #####
        
   
nlist=[5, 10, 20, 100,1000]  #list of dimensions to test
 
sol1={} #dictionaries for the values of xTx and xTMx to be printed into json
sol2={}

xTxrt=[] #arrays for the run times
Mxrt=[]
xTMxrt=[]
MMrt=[]

for i in range(len(nlist)):   #testing all the operations by looping through the 4 dimension sizes
    
    n=nlist[i]
    
    xT=Xt(n)
    x=X(xT)
    m= M(n)

            
    xtxs=time.perf_counter()
    xTx= int(dot(xT,x))
    xtx=time.perf_counter()
    xTxrt.append(xtx-xtxs)  #time taken to execute operation
                
    mxs=time.perf_counter()
    Mx = matrixmult(m,x)
    mx=time.perf_counter()
    Mxrt.append(mx-mxs)
    
    xtmxs=time.perf_counter()
    xTm=matrixmult(xT,m)
    xTMx = matrixmult(xTm,x)
    xtmx=time.perf_counter()   
    xTMxrt.append(xtmx-xtmxs)
         
                
    mms=time.perf_counter()
    MM = matrixmult(m,m)
    mm=time.perf_counter()
    MMrt.append(mm-mms)
    
    
    
    sol1[str(n)]= xTx
    sol2[str(n)]= int(xTMx[0][0])
    
    

sol={"xtx":sol1, "xtMx":sol2}

print(sol1)
print(sol2)

print(sol)

#creating a json file to write my solution into 
with open('cp_tut1_jrveri002.json','w') as f:
    json.dump(sol,f)


t1=[]
t2=[]
t3=[]
t4=[]

#using python modules
for i in range(len(nlist)):
    n=nlist[i]
    xT=Xt(n)
    x=X(xT)
    m= M(n)
    
    #dot product
    t1s=time.perf_counter()
    d=np.dot(xT,x)
    t1f=time.perf_counter()
    
    #Matrix multiplication
    t2s=time.perf_counter()
    mx=np.matmul(m,x)
    t2f=time.perf_counter()
    
    t3s=time.perf_counter()
    xtm=np.matmul(xT,m)
    xtmx=np.matmul(xtm,m)
    t3f=time.perf_counter()
    
    t4s=time.perf_counter()
    mm=np.matmul(m,m)
    t4f=time.perf_counter()
    
    t1.append(t1f-t1s)
    t2.append(t2f-t2s)
    t3.append(t3f-t3s)
    t4.append(t4f-t4s)
    
    








    



