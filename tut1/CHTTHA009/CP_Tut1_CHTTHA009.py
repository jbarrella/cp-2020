# thavish chetty - chttha009 - computational physics - tutorial 1
# section 1: importing modules
import numpy as np
import matplotlib.pyplot as plt
import timeit
import json


# section 2: define functions which calculates matrix elements and vector elements
def matrix_element(a, b):  # matrix function
    return ((37.1*a + 91.7*(b**2)) % 20.0) - 10.0


def vector_element(c):  # vector function
    return c


# section 4: creating functions to insert above elements into vector and matrix objects
def create_square_matrix(size):
    matrix = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(matrix_element(i + 1, j + 1))
        matrix.append(row)
    return matrix


def create_vector(size):
    vector = []
    for i in range(size):
        vector.append(vector_element(i + 1))
    return vector


# section 3: defining operators such as vector dot product, matrix-vector multiplication etc.
def dot_product(x, y):  # function which dots two vectors of equal lengths together
    summation = 0
    for k in range(len(x)):
        product = x[k]*y[k]
        summation = summation + product
    return summation


def matrix_multiply_vector(m, x):
    z = []
    for r in range(len(m[0])):
        product = dot_product(m[r], x)
        z.append(product)
    return z


def column(matrix, col_number):
    column_vector = []
    for row in matrix:
        column_vector.append(row[col_number])
    return column_vector


def matrix_multiply_matrix(left_matrix, right_matrix):
    new_matrix = []
    for i in range(len(left_matrix)):
        new_matrix_row = []
        for j in range(len(right_matrix)):
            right_matrix_column = column(right_matrix, j)
            new_matrix_row.append(dot_product(left_matrix[i], right_matrix_column))
        new_matrix.append(new_matrix_row)
    return new_matrix


# section 4: setting up dimensions of tensors for calculations plus initialising arrays for results
dimensions = [5, 10, 20, 100, 1000]
dimensionality = np.arange(10, 20 + 1, 10)
my_xtx_results = []
my_xtMx_results = []


# section 5: getting results for xtx and xtMx for n = 5, 10, 20, 100, 1000
for N in dimensions:
    x = create_vector(N)
    my_xtx = dot_product(x, x)
    my_xtx_results.append(my_xtx)
    M = create_square_matrix(N)
    my_xtMx = dot_product(x, matrix_multiply_vector(M, x))
    my_xtMx_results.append(my_xtMx)


# section 6: initializing arrays for runtime of manual matrix operations plus numpy package
my_xtx_time = []
my_xtMx_time = []
my_Mx_time = []
my_MM_time = []
numpy_xtx_time = []
numpy_xtMx_time = []
numpy_Mx_time = []
numpy_MM_time = []

# section 7: timimg runtime of all the operations for increasing vector dimension size
for num in dimensionality:
    vector = create_vector(num)
    numpy_vector = np.array(vector, dtype=np.int64)

    start_time = timeit.default_timer()
    my_xtx = dot_product(vector, vector)
    elapsed = timeit.default_timer() - start_time
    my_xtx_time.append(elapsed)
    start_time = timeit.default_timer()
    numpy_xtx = np.dot(numpy_vector, numpy_vector)
    elapsed = timeit.default_timer() - start_time
    numpy_xtx_time.append(elapsed)

    matrix = create_square_matrix(num)
    numpy_matrix = np.array(matrix)
    start_time = timeit.default_timer()
    my_xtMx = dot_product(vector, matrix_multiply_vector(matrix, vector))
    elapsed = timeit.default_timer() - start_time
    my_xtMx_time.append(elapsed)
    start_time = timeit.default_timer()
    numpy_xtMx = np.dot(numpy_vector, np.matmul(numpy_matrix, numpy_vector))
    elapsed = timeit.default_timer() - start_time
    numpy_xtMx_time.append(elapsed)

    start_time = timeit.default_timer()
    my_Mx = matrix_multiply_vector(matrix, vector)
    elapsed = timeit.default_timer() - start_time
    my_Mx_time.append(elapsed)
    start_time = timeit.default_timer()
    numpy_Mx = np.matmul(numpy_matrix, numpy_vector)
    elapsed = timeit.default_timer() - start_time
    numpy_Mx_time.append(elapsed)

    start_time = timeit.default_timer()
    my_MM = matrix_multiply_matrix(matrix, matrix)
    elapsed = timeit.default_timer() - start_time
    my_MM_time.append(elapsed)
    start_time = timeit.default_timer()
    numpy_MM = np.matmul(numpy_matrix, numpy_matrix)
    elapsed = timeit.default_timer() - start_time
    numpy_MM_time.append(elapsed)


# section 8:  calculating floating point operations per second(FLOPS) for each individual operation
tensor_dimension = 400
vec = create_vector(tensor_dimension)
mat = create_square_matrix(tensor_dimension)
FLOPS_data = []
operations = 2*tensor_dimension - 1
start_time = timeit.default_timer()
dot_product(vec, vec)
elapsed = timeit.default_timer() - start_time
FLOPS = operations/elapsed
FLOPS_data.append(FLOPS)
operations = 2*tensor_dimension**2 - tensor_dimension
start_time = timeit.default_timer()
matrix_multiply_vector(mat, vec)
elapsed = timeit.default_timer() - start_time
FLOPS = operations/elapsed
FLOPS_data.append(FLOPS)
operations = 2*tensor_dimension**2 + tensor_dimension - 1
start_time = timeit.default_timer()
dot_product(vec, matrix_multiply_vector(mat, vec))
elapsed = timeit.default_timer() - start_time
FLOPS = operations/elapsed
FLOPS_data.append(FLOPS)
operations = 2*tensor_dimension**3 - tensor_dimension**2
start_time = timeit.default_timer()
matrix_multiply_matrix(mat, mat)
elapsed = timeit.default_timer() - start_time
FLOPS = operations/elapsed
FLOPS_data.append(FLOPS)
print("FLOPS")
print(FLOPS_data)


# section 9: creating dictionary for output
output = {}
xtx = {}
xtMx = {}
for size in range(len(dimensions)):
    xtx.update({str(dimensions[size]): my_xtx_results[size]})
    xtMx.update({str(dimensions[size]): my_xtMx_results[size]})
output.update({"xtx": xtx})
output.update({"xtMx": xtMx})

# section 10: writing dictionary to a json file
with open('cp_tut1_chttha009.json', 'w') as f:
    json.dump(output, f)

# section 11s: plotting time :)
plt.figure(1)
plt.plot(dimensionality, my_xtx_time, linestyle='', marker='o', markersize=2, label="Manual Implementation")
plt.plot(dimensionality, numpy_xtx_time, linestyle='', marker='o',markersize=2,  label='Numpy Module')
plt.title("Plot comparing runtime of numpy library vs manual method of " + r'$\mathbf{x^{T}x}$')
plt.xlabel("Dimensionality of tensors")
plt.legend()
plt.ylabel("Runtime(seconds)")
plt.show()

plt.figure(1)
plt.plot(dimensionality, my_Mx_time, linestyle=' ', marker='o', markersize=2,   label="Manual Implementation")
plt.plot(dimensionality, numpy_Mx_time, linestyle='', marker='o',markersize=2,  label='Numpy Module')
plt.title("Plot comparing runtime of numpy library vs manual method of " + r'$\mathbf{Mx}$')
plt.xlabel("Dimensionality of tensors")
plt.legend()
plt.ylabel("Runtime(seconds)")
plt.show()

plt.figure(1)
plt.plot(dimensionality, my_xtMx_time, linestyle=' ', marker='o', markersize=2,  label="Manual Implementation")
plt.plot(dimensionality, numpy_xtMx_time, linestyle='', marker='o',markersize=2,    label='Numpy Module')
plt.title("Plot comparing runtime of numpy library vs manual method of " + r'$\mathbf{x^{T}Mx}$')
plt.xlabel("Dimensionality of tensors")
plt.legend()
plt.ylabel("Runtime(seconds)")
plt.show()

plt.figure(1)
plt.plot(dimensionality, my_MM_time, linestyle=' ', marker='o',markersize=2,  label="Manual Implementation")
plt.plot(dimensionality, numpy_MM_time, linestyle='', marker='o',markersize=2,   label='Numpy Module')
plt.title("Plot comparing runtime of numpy library vs manual method of " + r'$\mathbf{MM}$')
plt.xlabel("Dimensionality of tensors")
plt.legend()
plt.ylabel("Runtime(seconds)")
plt.show()


